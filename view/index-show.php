<html lang="ru">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="../CSS/main.css">
<?php include "navbar.php";?> <!–– Выводит навигационную панель––>
<div class="row">

    <?php

    foreach($conferences as $i)
    {
        ?>
        <div class="col-md-5">
            <div class="panel panel-primary">
                <div class="panel-heading">CONFERENCE</div>
                <div class="panel-body">
                    <?php
                    echo "Номер: ".$i['id']."<br>\n";
                    echo "Название: ".$i['title']."<br>\n";
                    echo "Дата: ".$i['data']."<br>\n";
                    echo "Адрес: ".$i['address']."<br>\n";
                    ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
